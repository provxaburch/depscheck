# README #

A little utility to help you locate mismatched assembly versions below a certain folder

### Usage ###

	DepsCheck <root-folder> [specific-assembly-name]
	Will print out version mismatch information about any assemblies found in any 'bin' folder below the root folder specified.
	Will optionally also print usage information for a specific assembly, useful for verification.

### Sample Output ###

Ran with parameters:

	λ DepsCheck c:\REPO\provantagex\server AnyConfig

Resulting in

	Scanning: c:\REPO\provantagex\server
	--------------SPECIFIC--------------
	Assembly: AnyConfig
		[1.0.112.0]
			c:/REPO/provantagex/server/ITN.Web/ITN.AutoAvails.Web/bin/AnyConfig.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.ePort.Gateway/bin/AnyConfig.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.ePort.Web/bin/AnyConfig.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.Web/bin/AnyConfig.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.Web.Admin/bin/AnyConfig.dll
	----------------MISMATCHES----------------
	Assembly: Microsoft.Extensions.Caching.Memory
		[3.1.3.0]
			c:/REPO/provantagex/server/ITN.Web/ITN.AutoAvails.Web/bin/Microsoft.Extensions.Caching.Memory.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.ePort.Gateway/bin/Microsoft.Extensions.Caching.Memory.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.ePort.Web/bin/Microsoft.Extensions.Caching.Memory.dll
		[3.1.7.0]
			c:/REPO/provantagex/server/ITN.Web/ITN.Web/bin/Microsoft.Extensions.Caching.Memory.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.Web.Admin/bin/Microsoft.Extensions.Caching.Memory.dll
	Assembly: Microsoft.Extensions.Configuration.Abstractions
		[3.1.7.0]
			c:/REPO/provantagex/server/ITN.Web/ITN.AutoAvails.Web/bin/Microsoft.Extensions.Configuration.Abstractions.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.Web/bin/Microsoft.Extensions.Configuration.Abstractions.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.Web.Admin/bin/Microsoft.Extensions.Configuration.Abstractions.dll
		[3.1.3.0]
			c:/REPO/provantagex/server/ITN.Web/ITN.ePort.Gateway/bin/Microsoft.Extensions.Configuration.Abstractions.dll
	Assembly: Microsoft.Extensions.DependencyInjection.Abstractions
		[3.1.3.0]
			c:/REPO/provantagex/server/ITN.Web/ITN.AutoAvails.Web/bin/Microsoft.Extensions.DependencyInjection.Abstractions.dll
		[3.1.7.0]
			c:/REPO/provantagex/server/ITN.Web/ITN.Web/bin/Microsoft.Extensions.DependencyInjection.Abstractions.dll
			c:/REPO/provantagex/server/ITN.Web/ITN.Web.Admin/bin/Microsoft.Extensions.DependencyInjection.Abstractions.dll