﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DepsCheck
{
    class Program
    {

        private class AssemblyInfo
        {
            public string Name { get; set; }
            public Dictionary<string, HashSet<string>> Versions { get; set; } = new Dictionary<string, HashSet<string>>();

            public AssemblyInfo(AssemblyName aName)
            {
                Add(aName);
            }

            public void Add(AssemblyName aName)
            {
                Name = aName.Name;
                var version = aName.Version.ToString();
                if (Versions.TryGetValue(version, out var existing))
                    existing.Add(aName.CodeBase);
                else
                    Versions.Add(version, new HashSet<string> {aName.CodeBase});
            }
        }

        private static readonly Dictionary<string, AssemblyInfo> _infos = new Dictionary<string, AssemblyInfo>();

        static void Main(string[] args)
        {
            string specific = null;
            if (args.Length < 1)
            {
                PrintUsage();
                return;
            }

            var rootPath = args[0];
            if (!Directory.Exists(rootPath))
            {
                Console.WriteLine($"Root Folder Not Found: {rootPath}");
                PrintUsage();
                return;
            }

            Console.WriteLine($"Scanning: {rootPath}");
            Scan(rootPath);

            if (args.Length > 1)
            {
                specific = args[1];
                Console.WriteLine("--------------SPECIFIC--------------");
                if (_infos.ContainsKey(specific))
                    PrintInfoFor(specific);
                else
                    Console.WriteLine($"Assembly {specific} was not found");
            }

            Console.WriteLine("----------------MISMATCHES----------------");

            var mismatches = _infos.Where(a => a.Value.Versions.Count > 1);
            foreach (var item in mismatches.Where(a => a.Key != specific))
                PrintInfoFor(item.Value);
        }

        private static void PrintUsage()
        {
            Console.WriteLine("USAGE: DepsCheck <root-folder> [specific-assembly-name]");
            Console.WriteLine("Will print out version mismatch information about any assemblies found in any 'bin' folder below the root folder specified.\r\nWill optionally also print usage information for a specific assembly, useful for verification.");
        }

        private static void PrintInfoFor(string name)
        {
            if(_infos.TryGetValue(name, out var info))
                PrintInfoFor(info);
            else
                Console.WriteLine($"Could not find assembly {name}");
        }

        private static void PrintInfoFor(AssemblyInfo info)
        {
            Console.WriteLine($"Assembly: {info.Name}");
            foreach (var version in info.Versions)
            {
                Console.WriteLine($"    [{version.Key}]");
                foreach (var file in version.Value)
                {
                    Console.WriteLine($"        {file.Replace("file:///","")}");
                }
            }
            
        }

        private static void Scan(string path)
        {
            if (!Directory.Exists(path))
                return;
            foreach (var sub in Directory.GetDirectories(path))
                Scan(sub);
            if (!Path.GetFileName(path).Equals("bin", StringComparison.OrdinalIgnoreCase))
                return;
            foreach (var file in Directory.GetFiles(path, "*.dll"))
            {
                try
                {
                    var aName = AssemblyName.GetAssemblyName(file);
                    if (_infos.TryGetValue(aName.Name, out var info))
                        info.Add(aName);
                    else
                        _infos.Add(aName.Name, new AssemblyInfo(aName));
                }
                catch (Exception e)
                {
                    Console.WriteLine($"ERR: {file} : {e.Message}");
                }
            }
        }
    }
}
